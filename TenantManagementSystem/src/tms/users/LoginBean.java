package tms.users;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class LoginBean {
	private String name;
	private String password;
	public final String getName() {
		return name;
	}
	public final void setName(final String name) {
		this.name = name;
	}
	public final String getPassword() {
		return password;
	}
	public final void setPassword(final String password) {
		this.password = password;
	}
	
	

}
