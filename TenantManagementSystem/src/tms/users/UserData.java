package tms.users;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;






@ManagedBean(name ="usrData")
@RequestScoped

public class UserData {
	
	private String name,surname,emailAddress,idNumber,race,gender,password;
	private int age,cellNumber;
	private String dateOfBirth;
	private String dbEmail,dbPassword;
	Connection con;
	//DataSource ds;
	private String password2;
	
	/*public UserData()
	{
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/database");
		}
		catch(NamingException e) {
		e.printStackTrace();	
		}
		
	}*/
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getDbEmail() {
		return dbEmail;
	}
	public void setDbEmail(String dbEmail) {
		this.dbEmail = dbEmail;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(int cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dataOfBirth) {
		this.dateOfBirth = dataOfBirth;
	}
	
	
	public String add() {
		int i = 0;
		if (name != null) {
		PreparedStatement ps = null;
		
		try {
		
			con = getConnection();
		
		
		String sql = "INSERT INTO userRegister(name,surname,dateOfBirth,idNumber,age,emailAddress,cellNumber,race,password) VALUES(?,?,?,?,?,?,?,?,?)";
		ps = con.prepareStatement(sql);
		ps.setString(1, name);
		ps.setString(2, surname);
		ps.setDate(3, java.sql.Date.valueOf(dateOfBirth));
		ps.setString(4, idNumber);
		ps.setInt(5, age);
		ps.setString(6, emailAddress);
		ps.setInt(7, cellNumber);
		ps.setString(8, race);
		ps.setString(9, password);
	
		
		i = ps.executeUpdate();
		System.out.println("Data Added Successfully");
		
		}
		 catch (Exception e) {
		System.out.println(e);
		} finally {
			try {
				
			//	ps.close();
				//con.close();
				} catch (Exception e) {
				e.printStackTrace();
				}
				}
				}
				if (i > 0) {
				return "success";
				} else
				return "unsuccess";
				}
		
	
	
	public void dbData(String emailAddress) throws ClassNotFoundException {
		if(emailAddress != null)
		{
			PreparedStatement ps = null;
			con = getConnection();
			ResultSet rs = null;
			
			try {
				getConnection();
			if (con != null) {
			String sql = "select name,surname,dateOfBirth,idNumber,emailAddress,age,cellNumber,race, password from userRegister where emailAddress = '"
			+ emailAddress + "'";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			name = rs.getString("name");
			surname = rs.getString("surname");
			Date dateOfBirth = rs.getDate("dateOfBirth");
			idNumber = rs.getString("idNumber");
			age = rs.getInt("age");
			cellNumber = rs.getInt("cellNumber");
			race = rs.getString("race");
			dbEmail = rs.getString("emailAddress");
			dbPassword = rs.getString("password");
			
									
			}
			} catch (SQLException | ClassNotFoundException sqle) {
			sqle.printStackTrace();
			}
			}
			
			
		}
		public String login() throws ClassNotFoundException {
			dbData(emailAddress);
			if (emailAddress.equals(dbEmail) && password.equals(dbPassword)) {
			return "success";
			} else
			return "login";
			}
		
		
		public void logout() {
			FacesContext.getCurrentInstance().getExternalContext()
			.invalidateSession();
			FacesContext.getCurrentInstance()
			.getApplication().getNavigationHandler()
			.handleNavigation(FacesContext.getCurrentInstance(), null, "/home.xhtml");
			}
		
		
		 public Connection getConnection() throws ClassNotFoundException{
			
			 Connection con = null;

		      String url = "jdbc:mysql://localhost:3306/tms";
		      String user = "root";
		      String password = "";
		      try {
		    	  Class.forName("com.mysql.jdbc.Driver"); 
					 
					 
		         con = DriverManager.getConnection(url, user, password);
		         System.out.println("Connection completed.");
		      } catch (SQLException ex) {
		         System.out.println(ex.getMessage());
		      }
		      finally{
		    	
		      }
		      return con;
		   }
		
	}
	
	
	
	


