/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.studentrecordmanagement.beans;



import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcus
 */
@ManagedBean
@SessionScoped
public class SimpleCrudBean implements Serializable {
    private static final long serialVersionID = 1L;
    private List<Student> list;
    private Student item = new Student();
    private Student beforeEditItem = null;
    private boolean edit;
    
    @PostConstruct
    public void inti()
    {
        list = new ArrayList<Student>();
    }
    public void add() {
        item.setId(list.isEmpty() ? 1 : list.get(list.size() - 1).getId() + 1);
        list.add(item);
        item = new Student();
    }
    
    public void resetAdd() {
        item = new Student();
    }
    
    public void edit(Student item) {
        beforeEditItem = item.clone();
        this.item = item;
        edit = true;
    }
    
    public void cancelEdit() {
        this.item.restore(beforeEditItem);
        this.item = new Student();
        edit = false;
    }
    
    public void saveEdit() {
        // DAO save the edit
        this.item = new Student();
        edit = false;
    }
    
    public void delete(Student item) throws IOException {
        // DAO save the delete
        list.remove(item);
    }
    
    public List<Student> getList() {
        return list;
    }
    
    public Student getItem() {
        return this.item;
    }
    
    public boolean isEdit() {
        return this.edit;
    }

       
}
