/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.studentrecordmanagement.beans;

import java.io.Serializable;

/**
 *
 * @author Marcus
 */
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String name;
    
    public Student() {}
    public Student(Long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public Student clone() {
        return new Student(id, name);
    
    }
    public void restore(Student student)
    {
        this.id = student.getId();
        this.name = student.getName();
    }
}
